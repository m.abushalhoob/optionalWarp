//
//  ViewController.swift
//  optionalWarp
//
//  Created by mohammad abushalhoob on 6/20/17.
//  Copyright © 2017 mohammad shalhoob. All rights reserved.
//
// https://medium.com/@JanLeMann/providing-meaningful-default-values-for-empty-or-absence-optionals-via-nil-or-empty-coalescing-379abd22ae77
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        var myOptional: MyStruct? = nil
        print(myOptional.orWhenNilOrEmpty(MyStruct(name:"Hello World1"))) //"Hello World"
        
        myOptional = MyStruct(name: "")
        print(myOptional.orWhenNilOrEmpty(MyStruct(name:"Hello World2"))) //"Hello World"
        
        myOptional = MyStruct(name: "Yeah")
        print(myOptional.orWhenNilOrEmpty(MyStruct(name:"Hello World"))) //"Yeah"
        
        let optionalString: String? = nil
        let mandatoryString = optionalString ??? "Default Value"
        print(mandatoryString)
    }
    
}

struct MyStruct: Emptyable, CustomStringConvertible {
    var name: String
    var isEmpty: Bool { return name.isEmpty }
    var description: String { return name }
    init(name: String) {
        self.name = name
    }
}
