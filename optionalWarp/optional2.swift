//
//  optional2.swift
//  optionalWarp
//
//  Created by mohammad abushalhoob on 6/20/17.
//  Copyright © 2017 mohammad shalhoob. All rights reserved.
//

protocol Emptyable {
    var isEmpty: Bool { get }
}

extension Optional where Wrapped: Emptyable {
    func orWhenNilOrEmpty(_ defaultValue: Wrapped) -> Wrapped {
        switch(self) {
        case .none:
            return defaultValue
        case .some(let value) where value.isEmpty:
            return defaultValue
        case .some(let value):
            return value
        }
    }
}

extension String: Emptyable {}

extension Array: Emptyable {}

infix operator ???: NilCoalescingPrecedence
extension Optional where Wrapped: Emptyable {
    static func ???(left: Wrapped?, right: Wrapped) -> Wrapped {
        return left.orWhenNilOrEmpty(right)
    }
}
